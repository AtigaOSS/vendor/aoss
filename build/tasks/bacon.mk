# Copyright (C) 2023 Atiga Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# -----------------------------------------------------------------
# AOSS OTA update package

AOSS_TARGET_PACKAGE := $(PRODUCT_OUT)/$(AOSS_DISPLAY).zip

.PHONY: bacon atiga
bacon: $(INTERNAL_OTA_PACKAGE_TARGET)
	$(hide) mv $(INTERNAL_OTA_PACKAGE_TARGET) $(AOSS_TARGET_PACKAGE)
	$(hide) $(MD5SUM) $(AOSS_TARGET_PACKAGE) | sed "s|$(PRODUCT_OUT)/||" > $(AOSS_TARGET_PACKAGE).md5sum
	@echo ""
	@echo -e "----------------------------------------------------------------------"
	@echo -e "   .d8b.  d888888b d888888b  d888b   .d8b.   .d88b.  .d8888. .d8888.  "
	@echo -e "  d8' `8b `~~88~~'   `88'   88' Y8b d8' `8b .8P  Y8. 88'  YP 88'  YP  "
	@echo -e "  88ooo88    88       88    88      88ooo88 88    88 `8bo.   `8bo.    "
	@echo -e "  88~~~88    88       88    88  ooo 88~~~88 88    88   `Y8b.   `Y8b.  "
	@echo -e "  88   88    88      .88.   88. ~8~ 88   88 `8b  d8' db   8D db   8D  "
	@echo -e "  YP   YP    YP    Y888888P  Y888P  YP   YP  `Y88P'  `8888Y' `8888Y'  "
	@echo -e "----------------------------------------------------------------------"
	@echo -e "Package path : "$(AOSS_TARGET_PACKAGE)
	@echo -e "Package md5  : `cat $(AOSS_TARGET_PACKAGE).md5sum | cut -d ' ' -f 1`"
	@echo -e "Package size : `du -h $(AOSS_TARGET_PACKAGE) | cut -f 1`"
	@echo -e ""

atiga: bacon

