#
# Copyright (C) 2023 Atiga Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#
# Handle various build version information.
#

ifndev AOSS_CODEREF
    AOSS_CODEREF := 1.0
endif

ifndef AOSS_CODENAME
    AOSS_CODENAME := RabiulAwwal
endif

ifndef AOSS_RELEASE
    AOSS_RELEASE := Community
endif

ifndef AOSS_BUILDTYPE
    AOSS_TYPE := Developers
else
    ifeq ($(strip $(AOSS_BUILDTYPE)), ALPHA)
        AOSS_TYPE := Alpha
    else ifeq ($(strip $(AOSS_BUILDTYPE)), BETA)
        AOSS_TYPE := Beta
    else ifeq ($(strip $(AOSS_BUILDTYPE)), RELEASE)
        AOSS_TYPE := Release
    endif
endif

# Append date to AtigaOSS zip name
ifndef AOSS_BUILD_DATE
    AOSS_BUILD_DATE := $(shell TZ=Asia/Jakarta date +"%a, %d %b %Y %H:%M:%S +0700")
endif

ifndef AOSS_BUILD_UTC
    AOSS_BUILD_UTC := $(shell date -u +%Y%m%d_%H%M%S)
endif

ifneq ($(filter Alpha Beta, $(AOSS_TYPE)),)
    AOSS_DISPLAY := AtigaOSS_$(AOSS_DEVICE)_$(AOSS_CODEREF)-$(AOSS_CODENAME)_$(AOSS_BUILD_UTC)_$(AOSS_RELEASE)
else
    AOSS_DISPLAY := AtigaOSS_$(AOSS_DEVICE)_$(AOSS_CODEREF)-$(AOSS_CODENAME)_$(AOSS_TYPE)_$(AOSS_BUILD_UTC)_$(AOSS_RELEASE)
endif

# AtigaOSS System Version
PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
    ro.aoss.version=$(AOSS_CODEREF) \
    ro.aoss.codename=$(AOSS_CODENAME)

# AtigaOSS Platform Display Version
PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
    ro.aoss.build_date=$(AOSS_BUILD_DATE) \
    ro.aoss.build_date_utc=$(AOSS_BUILD_UTC) \
    ro.aoss.build_release=$(AOSS_RELEASE) \
    ro.aoss.build_type=$(AOSS_TYPE)
