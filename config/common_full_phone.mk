IS_PHONE := true

# Inherit common AtigaOSS Project stuff
$(call inherit-product, vendor/aoss/config/common.mk)

$(call inherit-product, vendor/aoss/config/telephony.mk)
