LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE                := ViaBrowser
LOCAL_SRC_FILES             := app/ViaBrowser.apk
LOCAL_MODULE_TAGS           := optional
LOCAL_MODULE_CLASS          := APPS
LOCAL_CERTIFICATE           := PRESIGNED
LOCAL_MODULE_SUFFIX         := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_OVERRIDES_PACKAGES    := Browser Browser2
LOCAL_DEX_PREOPT            := false
include $(BUILD_PREBUILT)
